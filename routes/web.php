<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['guest']], function () {
  Route::get('/login', [LoginController::class, 'index'])->name('login');
  
  Route::post('authenticate', [LoginController::class, 'authenticate'])->name('authenticate');
});
  
Route::group(['middleware' => ['auth']], function () {
  Route::get('/', function () {
    return view('home');
  })->name('home');

  Route::get('/home', function () {
      return redirect()->route('home');
  });

  Route::get('logout', [LoginController::class, 'logout'])->name('logout');

  Route::resource('user', UserController::class);
  Route::resource('author', AuthorController::class);
  Route::get('book/getAuthors', [BookController::class, 'getAuthors'])->name('book.get-authors');
  Route::resource('book', BookController::class);
});

