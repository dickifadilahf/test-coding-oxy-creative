@extends('layouts.app')

@section('title', $title)

@push('vendor-styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
<div class="card">
  <div class="card-header">
    <div class="row">
      <div class="col-md-10">
        <h3>{{ $title }}</h3>
      </div>
      <div class="col-md-2 text-right">
        <button class="btn btn-primary" id="btn-create">Create</button>
       </div>
    </div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table id="book-list-table" class="table nowrap">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th class="text-center">Number of Pages</th>
            <th class="text-center">Total Author</th>
            <th class="text-right">Actions</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal" id="modal-form">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span id="modal-form-title-pre"></span> {{ $title }}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form id="form">
        <input type="text" id="form-id" name="id" style="display: none">
        <div class="modal-body">
          <div class="row justify-content-center">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="form-title"><small class="text-danger">* </small>Title</label>
                <input type="text" class="form-control" id="form-title" name="title" required autocomplete="off">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label for="form-number-of-pages"><small class="text-danger">* </small>Number of Pages</label>
                <input type="number" min="1" class="form-control" id="form-number-of-pages" name="number_of_pages" required autocomplete="off">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group">
                    <label for="form-author">Author <span class="text-muted">(Select at least one Author)</span></label>
                    <select class="form-control" id="form-author" data-width="100%">
                      <option value=""></option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="btn-author-add"></label>
                    <button class="btn btn-secondary" id="btn-author-add">Add</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="table-border-style">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="modal-form-authors">
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btn-form-save">
						<span class="spinner-border spinner-border-sm" role="status"></span>
            <span class="load-text">Saving...</span>
						<span class="btn-text">Save</span>
          </button>
          <button type="reset" class="btn btn-danger" id="btn-form-clear" style="display: none">Clear</button>
          <button class="btn btn-danger" id="btn-form-cancel" style="display: none">Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-show" tabindex="-1" role="dialog" aria-labelledby="modal-show-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-show-label">Show {{ $title }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="show-title">Title</label>
              <input type="text" readonly class="form-control" id="show-title" placeholder="">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label for="show-number-of-pages">Number of Pages</label>
              <input type="text" readonly class="form-control" id="show-number-of-pages" placeholder="Number of Pages">
            </div>
          </div>
          <div class="col-sm-12">
            <p>Authors</p>
            <div class="table-border-style">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody id="modal-show-authors">
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" id="btn-show-close">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endpush

@push('scripts')
<script>
  $(document).ready(function () {    
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    let datatable = $('#book-list-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: "{{ route($route.'index') }}",
      columns: [
        {data: 'DT_RowIndex', orderable: false, searchable: false},
        {data: 'title'},
        {data: 'number_of_pages', render: (data) => {
          return `
          <div class="text-center">
            ${data}
          </div>
          `;
        }},
        {data: 'total_author', orderable: false, searchable: false, render: (data) => {
          return `
          <div class="text-center">
            ${data}
          </div>
          `;
        }},
        {data: 'id', render: (data) => {
          let html = `
            <div class="text-right">
              <button type="button" class="btn btn-sm btn-secondary btn-show" data-id="${data}">
                <span class="spinner-border spinner-border-sm" role="status" style="display: none"></span>
                Show
              </button>
              <button type="button" class="btn btn-sm btn-info btn-edit" data-id="${data}">
                <span class="spinner-border spinner-border-sm" role="status" style="display: none"></span>
                Edit
              </button>
              @hasrole('Admin')
              <button type="button" class="btn btn-sm btn-danger btn-delete" data-id="${data}">
                <span class="spinner-border spinner-border-sm" role="status" style="display: none"></span>
                Delete
              </button>
              @endhasrole
            </div>
          `;

          return html;
        }, orderable: false, searchable: false},
      ],
      order: [[1, 'asc']],
    });

    let authorExist = [];

    $('#form').bind('reset', function () {
      $(this).find('#form-author').val('').trigger('change');
      authorExist = [];
      $('#modal-form-authors').empty();
    });

    $('#modal-form').on('shown.bs.modal', function () {
      $(this).find('#form-title').focus();
    });

    $('#modal-form').on('hidden.bs.modal', function () {
      $('#form').trigger('reset');
    });

    $('#btn-create').click(function () {
      $('#form').trigger('reset');
      $('#modal-form-title-pre').html('Create');
      $('#btn-form-clear').show();
      $('#btn-form-cancel').hide();
      $('#modal-form').modal('show');
    });

    $('#btn-form-save').each(function () {
      $(this).children('.spinner-border').hide();
      $(this).children('.load-text').hide();
    });

    $('#btn-form-cancel').click(function (e) {
      e.preventDefault();
      $('#modal-form').modal('toggle');
    });

    $('#form-author').select2({
      dropdownParent: $("#modal-form"),
      allowClear: true,
      placeholder: 'Select Author',
      ajax: {
        url: "{{ route($route.'get-authors') }}",
        dataType: 'json',
        delay: 250,
        data: function(params) {
          return {
            q: params.term,
            page: params.page
          };
        },
        processResults: function(response, params) {
          params.page = params.page || 1;

          return {
            results: $.map(response.data, function (item) {
              return {
                text: item.name,
                id: item.id
              }
            }),
            pagination: {
              more: (params.page * 30) < response.data.length
            }
          };
        },
        cache: true,
      },
    });

    $('#btn-author-add').click(function (e) { 
      e.preventDefault();

      if ($('#form-author').val() != "") {
        let id = $('#form-author').val();
        let name = $("#form-author option:selected").text();

        if(jQuery.inArray(id, authorExist) == -1) {
          let html = `
          <tr id="author-${id}">
            <input type="hidden" name="author_id[]" value="${id}">
            <td>${name}</td>
            <td class="text-center">
              <a href="#" class="btn btn-icon btn-sm btn-danger btn-author-delete" data-id="${id}">Delete</a>
            </td>
          </tr>
          `;
          
          authorExist.push(id.toString());
          $('#modal-form-authors').append(html);
          $('#form-author').val('').trigger('change');
        } else {
          swal({
            title: 'Add failed!',
            text: 'Author already exist.',
            icon: "error",
            timer: 3000,
          });
        }
      }

    });

    $(document).on("click", ".btn-author-delete", function () {
      let id = $(this).data('id');

      swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $('#author-'+id).remove();

          authorExist = jQuery.grep(authorExist, function(value) {
            return value != id.toString();
          }); 
        }
      });
    });

    $('#form').submit(function (e) {
      e.preventDefault();
      
      let btnFormSave = $('#btn-form-save');
      let url;
      let type;

      if (authorExist.length > 0) {
        if ($('#form-id').val().length === 0) {
          url = "{{ route($route.'store') }}";
          type = 'POST';
        } else {
          url = "{{ route($route.'update', ':id') }}";
          url = url.replace(':id', $('#form-id').val());
          type = 'PATCH';
        }

        $.ajax({
          data: $('#form').serialize(),
          url: url,
          type: type,
          dataType: 'json',
          beforeSend: function () {
            btnFormSave.children('.spinner-border').show();
            btnFormSave.children('.load-text').show();
            btnFormSave.children('.btn-text').hide();
          },
          success: function (response) {
            btnFormSave.children('.spinner-border').hide();
            btnFormSave.children('.load-text').hide();
            btnFormSave.children('.btn-text').show();

            $('#form').trigger("reset");
            $('#modal-form').modal('toggle');

            datatable.draw();
            
            swal({
              title: response.swal.title,
              text: response.swal.text,
              icon: response.swal.icon,
              timer: response.swal.timer,
            });
          },
          error: function (response) {
            btnFormSave.children('.spinner-border').hide();
            btnFormSave.children('.load-text').hide();
            btnFormSave.children('.btn-text').show();

            swal({
              title: 'Save failed!',
              text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
              icon: 'error',
              timer: 3000,
            });
          }
        });
      } else {
        swal({
          title: 'Save failed!',
          text: 'Select at least one Authors',
          icon: 'warning',
          timer: 3000,
        });
      }
    });

    $(document).on("click", ".btn-edit", function () {
      let port = $(this);
      let url = "{{ route($route.'edit', ':id') }}";
      url = url.replace(':id', $(this).data('id'));

      $.ajax({
        url: url,
        type: "GET",
        dataType: 'json',
        beforeSend: function () {
          port.children('.spinner-border').show();
        },
        success: function (response) {
          port.children('.spinner-border').hide();

          $('#form-id').val(response.data.id);
          $('#form-title').val(response.data.title);
          $('#form-number-of-pages').val(response.data.number_of_pages);

          let html = ``;
          if (response.data.authors) {
            response.data.authors.forEach(e => {
              console.log(e.name);
              html += `
              <tr id="author-${e.id}">
                <input type="hidden" name="author_id[]" value="${e.id}">
                <td>${e.name}</td>
                <td class="text-center">
                  <a href="#" class="btn btn-icon btn-sm btn-danger btn-author-delete" data-id="${e.id}">Delete</a>
                </td>
              </tr>
              `;
              
              authorExist.push(e.id.toString());
            });

            $('#modal-form-authors').append(html);
          }

          $('#modal-form-title-pre').html('Edit');
          $('#btn-form-cancel').show();
          $('#btn-form-clear').hide();

          $('#modal-form').modal('show');
        },
        error: function (response) {
          port.children('.spinner-border').hide();
          
          swal({
            title: 'Edit failed!',
            text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
            icon: "error",
            timer: 3000,
          });
        },
      });
    });

    @hasrole('Admin')
    $(document).on("click", ".btn-delete", function () {
      let port = $(this);
      let id = $(this).data('id');
      let url = "{{ route($route.'destroy', ':id') }}";
      url = url.replace(':id', $(this).data('id'));

      swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            url: url,
            type: "DELETE",
            dataType: 'json',
            beforeSend: function () {
              port.children('.spinner-border').show();
            },
            success: function (response) {
              port.children('.spinner-border').hide();

              datatable.draw();

              swal({
                title: response.swal.title,
                text: response.swal.text,
                icon: response.swal.icon,
                timer: response.swal.timer,
              });
            },
            error: function (response) {
              port.children('.spinner-border').hide();

              swal({
                title: 'Delete failed!',
                text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
                icon: "error",
                timer: 3000,
              });
            },
          });
        }
      });
    });
    @endhasrole

    $(document).on("click", ".btn-show", function () {
      let port = $(this);
      let url = "{{ route($route.'show', ':id') }}";
      url = url.replace(':id', $(this).data('id'));

      $.ajax({
        url: url,
        type: "GET",
        dataType: 'json',
        beforeSend: function () {
          port.children('.spinner-border').show();
        },
        success: function (response) {
          port.children('.spinner-border').hide();

          $('#show-title').val(response.data.title);
          $('#show-number-of-pages').val(response.data.number_of_pages);

          $('#modal-show-authors').empty();
          let html = ``;
          if (response.data.authors.length > 0) {
            response.data.authors.forEach(e => {
              html += `
              <tr>
                <td>${e.name}</td>
              <tr>
              `;
            });
          } else {
            html = `
            <tr>
              <td>There is no authors</td>
            <tr>
            `;
          }
          $('#modal-show-authors').html(html);

          $('#modal-show').modal('show');
        },
        error: function (response) {
          port.children('.spinner-border').hide();

          swal({
            title: 'Show failed!',
            text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
            icon: "error",
            timer: 3000,
          });
        },
      });
    });

    $('#btn-show-close').click(function () {
      $('#modal-show').modal('toggle');
    });
  });
</script>
@endpush