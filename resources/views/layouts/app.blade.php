<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title', 'Dashboard') | {{ config('app.name') }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <style>
    .swal-footer {
      text-align: center;
    }
  </style>
  @stack('vendor-styles')
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="{{ route('home') }}">Books</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav mr-auto">
      @hasrole('Admin')
      <li class="nav-item">
        <a class="nav-link {{ Route::is('user.*') ? 'active' : '' }}" href="{{ route('user.index') }}">User</a>
      </li>
      @endhasrole
      <li class="nav-item">
        <a class="nav-link {{ Route::is('author.*') ? 'active' : '' }}" href="{{ route('author.index') }}">Author</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Route::is('book.*') ? 'active' : '' }}" href="{{ route('book.index') }}">Book</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="#">Hello <strong>{{ Auth::user()->name }}</strong></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}">Logout</a>
      </li>
    </ul>
  </div>  
</nav>

<div class="container" style="margin-top:30px">
  <div class="row">
    <div class="col-sm-12">
      @yield('content')
    </div>
  </div>
</div>

<div class="text-center" style="margin-bottom:0;">
  <p>&copy; Dicki Fadilah Fajar</p>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@stack('plugin-scripts')
@stack('scripts')

</body>
</html>