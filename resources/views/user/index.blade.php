@extends('layouts.app')

@section('title', $title)

@push('vendor-styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
<div class="card">
  <div class="card-header">
    <div class="row">
      <div class="col-md-10">
        <h3>{{ $title }}</h3>
      </div>
      <div class="col-md-2 text-right">
        <button class="btn btn-primary" id="btn-create">Create</button>
       </div>
    </div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table id="book-list-table" class="table nowrap">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Level</th>
            <th class="text-right">Actions</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal" id="modal-form">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span id="modal-form-title-pre"></span> {{ $title }}</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form id="form">
        <input type="text" id="form-id" name="id" style="display: none">
        <div class="modal-body">
          <div class="row justify-content-center">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="form-name"><small class="text-danger">* </small>Name</label>
                <input type="text" class="form-control" id="form-name" name="name" required autocomplete="off">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label for="form-email"><small class="text-danger">* </small>Email</label>
                <input type="email" class="form-control" id="form-email" name="email" required autocomplete="off">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group" id="form-group-password">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label for="form-role"><small class="text-danger">* </small>Level</label>
                <select name="role" class="custom-select" id="form-role" required>
                  <option value="" disabled selected>Select Level</option>
                  @foreach ($roles as $role)
                  <option value="{{ $role->name }}">{{ $role->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="btn-form-save">
						<span class="spinner-border spinner-border-sm" role="status"></span>
            <span class="load-text">Saving...</span>
						<span class="btn-text">Save</span>
          </button>
          <button type="reset" class="btn btn-danger" id="btn-form-clear" style="display: none">Clear</button>
          <button class="btn btn-danger" id="btn-form-cancel" style="display: none">Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-show" tabindex="-1" role="dialog" aria-labelledby="modal-show-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-show-label">Show {{ $title }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="show-name">Name</label>
              <input type="text" readonly class="form-control" id="show-name" placeholder="Name">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label for="show-email">Email</label>
              <input type="text" readonly class="form-control" id="show-email" placeholder="Email">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label for="show-role">Level</label>
              <input type="text" readonly class="form-control" id="show-role" placeholder="Email">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger" id="btn-show-close">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
@endpush

@push('scripts')
<script>
  $(document).ready(function () {    
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    let datatable = $('#book-list-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: "{{ route($route.'index') }}",
      columns: [
        {data: 'DT_RowIndex', orderable: false, searchable: false},
        {data: 'name'},
        {data: 'email'},
        {data: 'id', orderable: false, searchable: false, render: function(data, type, full) {
          let html = ``;
          full.roles.forEach(e => {
            html += e.name;
          });
          return html;
        }},
        {data: 'id', render: (data) => {
          let html = `
            <div class="text-right">
              <button type="button" class="btn btn-sm btn-secondary btn-show" data-id="${data}">
                <span class="spinner-border spinner-border-sm" role="status" style="display: none"></span>
                Show
              </button>
              <button type="button" class="btn btn-sm btn-info btn-edit" data-id="${data}">
                <span class="spinner-border spinner-border-sm" role="status" style="display: none"></span>
                Edit
              </button>
              <button type="button" class="btn btn-sm btn-danger btn-delete" data-id="${data}">
                <span class="spinner-border spinner-border-sm" role="status" style="display: none"></span>
                Delete
              </button>
            </div>
          `;

          return html;
        }, orderable: false, searchable: false},
      ],
      order: [[1, 'asc']],
    });

    $('#modal-form').on('shown.bs.modal', function () {
      $(this).find('#form-name').focus();
    });

    $('#modal-form').on('hidden.bs.modal', function () {
      $('#form').trigger('reset');
    });

    $('#btn-create').click(function () {
      $('#form-group-password').html(`
      <label for="form-password"><small class="text-danger">* </small>Password</label>
      <input type="password" class="form-control" id="form-password" name="password" required autocomplete="off">
      `);
      $('#form').trigger('reset');
      $('#modal-form-title-pre').html('Create');
      $('#btn-form-clear').show();
      $('#btn-form-cancel').hide();
      $('#modal-form').modal('show');
    });

    $('#btn-form-save').each(function () {
      $(this).children('.spinner-border').hide();
      $(this).children('.load-text').hide();
    });

    $('#btn-form-cancel').click(function (e) {
      e.preventDefault();
      $('#modal-form').modal('toggle');
    });

    $('#form').submit(function (e) {
      e.preventDefault();
      
      let btnFormSave = $('#btn-form-save');
      let url;
      let type;

      if ($('#form-id').val().length === 0) {
        url = "{{ route($route.'store') }}";
        type = 'POST';
      } else {
        url = "{{ route($route.'update', ':id') }}";
        url = url.replace(':id', $('#form-id').val());
        type = 'PATCH';
      }

      $.ajax({
        data: $('#form').serialize(),
        url: url,
        type: type,
        dataType: 'json',
        beforeSend: function () {
          btnFormSave.children('.spinner-border').show();
          btnFormSave.children('.load-text').show();
          btnFormSave.children('.btn-text').hide();
        },
        success: function (response) {
          btnFormSave.children('.spinner-border').hide();
          btnFormSave.children('.load-text').hide();
          btnFormSave.children('.btn-text').show();

          $('#form').trigger("reset");
          $('#modal-form').modal('toggle');

          datatable.draw();
          
          swal({
            title: response.swal.title,
            text: response.swal.text,
            icon: response.swal.icon,
            timer: response.swal.timer,
          });
        },
        error: function (response) {
          btnFormSave.children('.spinner-border').hide();
          btnFormSave.children('.load-text').hide();
          btnFormSave.children('.btn-text').show();

          swal({
            title: 'Save failed!',
            text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
            icon: 'error',
            timer: 3000,
          });
        }
      });
    });

    $(document).on("click", ".btn-edit", function () {
      let port = $(this);
      let url = "{{ route($route.'edit', ':id') }}";
      url = url.replace(':id', $(this).data('id'));

      $.ajax({
        url: url,
        type: "GET",
        dataType: 'json',
        beforeSend: function () {
          port.children('.spinner-border').show();
        },
        success: function (response) {
          port.children('.spinner-border').hide();

          $('#form-group-password').html(`
          <label for="form-password">Password <span class="text-muted">(Fill to Reset Password)</span></label>
          <input type="password" class="form-control" id="form-password" name="password" autocomplete="off">
          `);

          $('#form-id').val(response.data.id);
          $('#form-name').val(response.data.name);
          $('#form-email').val(response.data.email);
          if (response.data.roles[0]) {
            $('#form-role').val(response.data.roles[0].name).change();
          }

          $('#modal-form-title-pre').html('Edit');
          $('#btn-form-cancel').show();
          $('#btn-form-clear').hide();

          $('#modal-form').modal('show');
        },
        error: function (response) {
          port.children('.spinner-border').hide();
          
          swal({
            title: 'Edit failed!',
            text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
            icon: "error",
            timer: 3000,
          });
        },
      });
    });

    $(document).on("click", ".btn-delete", function () {
      let port = $(this);
      let id = $(this).data('id');
      let url = "{{ route($route.'destroy', ':id') }}";
      url = url.replace(':id', $(this).data('id'));

      swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            url: url,
            type: "DELETE",
            dataType: 'json',
            beforeSend: function () {
              port.children('.spinner-border').show();
            },
            success: function (response) {
              port.children('.spinner-border').hide();

              datatable.draw();

              swal({
                title: response.swal.title,
                text: response.swal.text,
                icon: response.swal.icon,
                timer: response.swal.timer,
              });
            },
            error: function (response) {
              port.children('.spinner-border').hide();

              swal({
                title: 'Delete failed!',
                text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
                icon: "error",
                timer: 3000,
              });
            },
          });
        }
      });
    });

    $(document).on("click", ".btn-show", function () {
      let port = $(this);
      let url = "{{ route($route.'show', ':id') }}";
      url = url.replace(':id', $(this).data('id'));

      $.ajax({
        url: url,
        type: "GET",
        dataType: 'json',
        beforeSend: function () {
          port.children('.spinner-border').show();
        },
        success: function (response) {
          port.children('.spinner-border').hide();

          $('#show-name').val(response.data.name);
          $('#show-email').val(response.data.email);
          $('#show-role').val(response.data.roles[0].name);

          $('#modal-show').modal('show');
        },
        error: function (response) {
          port.children('.spinner-border').hide();

          swal({
            title: 'Show failed!',
            text: response.responseJSON ? response.responseJSON.message : 'Something went wrong.',
            icon: "error",
            timer: 3000,
          });
        },
      });
    });

    $('#btn-show-close').click(function () {
      $('#modal-show').modal('toggle');
    });
  });
</script>
@endpush