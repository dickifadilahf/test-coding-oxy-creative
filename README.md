Main Goal

The task is to create a simple application to manage Books based on Authors. 

Required Technology

•	Latest stable Bootstrap

•	Latest stable Laravel 


Requirements

•	Use basic Bootstrap to create simple Admin Control Panel.

•	Create Users Management with Admin and Non-Admin User Level.

•	Create Books Management.

•	Create Authors Management.

•	Create Login, Logout and Password Reset function.

•	Authors and Books should have a many-to-many database table relationship.

•	When creating or editing a Book, User should be able to select more than one Authors. 

Try to use https://select2.org/ or https://selectize.github.io/selectize.js/. 

•	Each Model landing page should have a table with list of record and action buttons.

•	Create at least ten dummy records for each Model.

•	Only Admin level can delete records.

•	Create a new Git and README.md using this document before starting project.

•	Push completed script to Git including database migration.


*Management = CRUD

General Instruction

•	You have eight hours to finish test. 

•	You have one hour lunch period. Pick your own hour.

•	You are free to use Google or ask friends if you have questions.

•	Prioritize a bug free application instead of a finished broken application.


## About
Framework : Laravel 8

Database : PostgreSQL

Frontend : Bootstrap 4

## How to install
Create database with name books_management

Copy .env.example to .env to setting your environment

Setup your database environment

## Run this command below after step above
composer update

php artisan key:generate

php artisan migrate

php artisan db:seed

## Sample User
Email = admin@example.com

Password = admin



Email = nonadmin@example.com

Password = nonadmin

## How to reset password
Open User menu, select User, after that fill password field to reset password.