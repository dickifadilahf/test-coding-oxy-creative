<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            [ 'name' => "Najwa Suryatmi M.Pd", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Indah Fujiati S.E.I", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Kenari Saragih", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Elisa Yuliarti", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Lurhur Pratama", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Bakiman Prakasa", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Ayu Hartati M.Pd", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Michelle Winarsih M.Farm", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Marsito Irnanto Prakasa S.Farm", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Mustika Budi Manullang", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Lutfan Pranowo S.Gz", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Cinta Ratna Purnawati M.Ak", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Tasnim Budiman", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Salman Suwarno", 'created_at' => now(), 'updated_at' => now(),],
            [ 'name' => "Lembah Sabri Nababan S.E.I", 'created_at' => now(), 'updated_at' => now(),],
        ];

        $books = [
            [ 'title' => "Boy Of Stardust", 'number_of_pages' => 78, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Soldier Of Earth's Legacy", 'number_of_pages' => 90, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Humans Of The Future", 'number_of_pages' => 50, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Girls Of Aliens", 'number_of_pages' => 250, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Agents And Creatures", 'number_of_pages' => 300, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Women And Guardians", 'number_of_pages' => 150, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Rise Of The Vacuum", 'number_of_pages' => 80, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Corruption Of Our Ship", 'number_of_pages' => 75, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Life In My Android Servant", 'number_of_pages' => 225, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Joy Of Eternity", 'number_of_pages' => 324, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Agent With Spaceships", 'number_of_pages' => 124, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Guest Of The Vacuum", 'number_of_pages' => 89, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Mercenaries Of The Orbit", 'number_of_pages' => 64, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Strangers With Spaceships", 'number_of_pages' => 13, 'created_at' => now(), 'updated_at' => now(), ],
            [ 'title' => "Visitors And Robots", 'number_of_pages' => 54, 'created_at' => now(), 'updated_at' => now(), ],
        ];

        Author::insert($authors);
        Book::insert($books);

        $users = [
            [
                'name'       => 'Admin',
                'email'      => 'admin@example.com',
                'password'   => bcrypt('admin'),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name'       => 'Non Admin',
                'email'      => 'nonadmin@example.com',
                'password'   => bcrypt('nonadmin'),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];
        User::insert($users);

        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'Non Admin']);

        $userAdmin = User::where('email', 'admin@example.com')->first();
        $userAdmin->assignRole('Admin');

        $userNonAdmin = User::where('email', 'nonadmin@example.com')->first();
        $userNonAdmin->assignRole('Non Admin');

        $indexAuthor = 0;
        foreach ($authors as $author) {
            $newAuthor = Author::where('name', $author['name'])->first();
            $newBook = Book::where('title', $books[$indexAuthor]['title'])->first();

            $newAuthor->books()->attach($newBook->id);
            $indexAuthor++;
        }

        $indexBook = 14;
        foreach ($books as $book) {
            $newBook = Book::where('title', $book['title'])->first();
            $newAuthor = Author::where('name', $authors[$indexBook]['name'])->first();

            $newBook->authors()->attach($newAuthor->id);
            $indexBook--;
        }
    }
}
