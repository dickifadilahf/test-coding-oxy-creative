<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class BookController extends Controller
{
  protected $model;
  protected $title = 'Book';
  protected $view  = 'book.';
  protected $route = 'book.';

  public function __construct(Book $model)
  {
    $this->model = $model;

    View::share('title', $this->title);
    View::share('view', $this->view);
    View::share('route', $this->route);

    $this->middleware(['role:Admin'])->only(['destroy']);
  }

  public function index(Request $request)
  {
    if($request->ajax()) {
      $data = $this->model->query();

      return DataTables::eloquent($data)
        ->addIndexColumn()
        ->make(true);
    }

    return view($this->view.'index');
  }

  public function create()
  {

  }

  public function store(Request $request)
  {
    DB::beginTransaction();
    try {
      $input = $request->only([
        'title',
        'number_of_pages',
      ]);
      $inputAuthorId = $request->input('author_id');
      
      $data = $this->model->create($input);

      $data->authors()->attach($inputAuthorId);
      
      $response = [
        'status'  => 'success',
        'message' => "$this->title created successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Created!',
          'text'  => $this->title.' created successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();

      return response()->json($response, 201);
    } catch (Exception $e) {
      DB::rollback();

      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function show($id)
  {
    $data = $this->model->with('authors')->findOrFail($id);

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }

  public function edit($id)
  {
    $data = $this->model->with('authors')->findOrFail($id);

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }

  public function update(Request $request, $id)
  {
    DB::beginTransaction();
    try {
      $input = $request->only([
        'title',
        'number_of_pages',
      ]);
      $inputAuthorId = $request->input('author_id');
      
      $data = $this->model->findOrFail($id);
      $bookAuthorIds = $data->authors()->pluck('id')->toArray();

      $attachAuthors = [];
      foreach ($inputAuthorId as $authorId) {
        if (!in_array($authorId, $bookAuthorIds)) {
          $attachAuthors[] = $authorId;
        }
      }
      
      $detachAuthors = [];
      foreach ($bookAuthorIds as $bookAuthorId) {
        if (!in_array($bookAuthorId, $inputAuthorId)) {
          $detachAuthors[] = $bookAuthorId;
        }
      }
  
      $data->update($input);

      if (count($attachAuthors) > 0 || count($detachAuthors) > 0) {
        $data->authors()->attach($attachAuthors);
        $data->authors()->detach($detachAuthors);
      }
  
      $response = [
        'status'  => 'success',
        'message' => "$this->title edited successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Edited!',
          'text'  => $this->title.' edited successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();
  
      return response()->json($response, 200);
    } catch (Exception $e) {
      DB::rollback();
    
      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function destroy($id)
  {
    DB::beginTransaction();
    try {
      $data = $this->model->findOrFail($id);
    
      $data->authors()->detach();
      $data->delete();

      $response = [
        'status'  => 'success',
        'message' => "$this->title deleted successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Deleted!',
          'text'  => $this->title.' deleted successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();
      
      return response()->json($response, 200);
    } catch (Exception $e) {
      DB::rollback();
    
      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function getAuthors(Request $request)
  {
    $data = Author::where(function ($q) use ($request) {
      if ($request->has('q')) {
        $keyword = $request->q;

        $q->where('name', 'LIKE', "%$keyword%");
      }
    })->get();

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }
}
