<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
  protected $model;
  protected $title = 'User';
  protected $view  = 'user.';
  protected $route = 'user.';

  public function __construct(User $model)
  {
    $this->model = $model;

    View::share('title', $this->title);
    View::share('view', $this->view);
    View::share('route', $this->route);

    $this->middleware(['role:Admin']);
  }

  public function index(Request $request)
  {
    $roles = Role::all();

    if($request->ajax()) {
      $data = $this->model->with('roles');

      return DataTables::eloquent($data)
        ->addIndexColumn()
        ->make(true);
    }

    return view($this->view.'index', compact('roles'));
  }

  public function create()
  {

  }

  public function store(Request $request)
  {
    DB::beginTransaction();
    try {
      $input = $request->only([
        'name',
        'email',
        'password',
      ]);
      $input['password'] = bcrypt($input['password']);
      $inputRole = $request->input('role');
      
      $data = $this->model->create($input);

      $user = $this->model->findOrFail($data->id);
      $user->assignRole($inputRole);
      
      $response = [
        'status'  => 'success',
        'message' => "$this->title created successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Created!',
          'text'  => $this->title.' created successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();

      return response()->json($response, 201);
    } catch (Exception $e) {
      DB::rollback();

      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function show($id)
  {
    $data = $this->model->with('roles')->findOrFail($id);

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }

  public function edit($id)
  {
    $data = $this->model->with('roles')->findOrFail($id);

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }

  public function update(Request $request, $id)
  {
    DB::beginTransaction();
    try {
      $input = $request->only([
        'name',
        'email',
        'password',
      ]);
      if ($input['password']) {
        $input['password'] = bcrypt($input['password']);
      } else {
        unset($input['password']);
      }
      $inputRole = $request->input('role');
      
      $data = $this->model->findOrFail($id);

      $data->update($input);

      DB::table('model_has_roles')->where('model_id', $id)->delete();
      $data->assignRole($inputRole);
  
      $response = [
        'status'  => 'success',
        'message' => "$this->title edited successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Edited!',
          'text'  => $this->title.' edited successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();
  
      return response()->json($response, 200);
    } catch (Exception $e) {
      DB::rollback();
    
      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function destroy($id)
  {
    DB::beginTransaction();
    try {
      $data = $this->model->findOrFail($id);
    
      $data->delete();

      $response = [
        'status'  => 'success',
        'message' => "$this->title deleted successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Deleted!',
          'text'  => $this->title.' deleted successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();
      
      return response()->json($response, 200);
    } catch (Exception $e) {
      DB::rollback();
    
      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }
}
