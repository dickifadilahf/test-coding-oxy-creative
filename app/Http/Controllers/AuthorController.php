<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class AuthorController extends Controller
{
  protected $model;
  protected $title = 'Author';
  protected $view  = 'author.';
  protected $route = 'author.';

  public function __construct(Author $model)
  {
    $this->model = $model;

    View::share('title', $this->title);
    View::share('view', $this->view);
    View::share('route', $this->route);

    $this->middleware(['role:Admin'])->only(['destroy']);
  }

  public function index(Request $request)
  {
    if($request->ajax()) {
      $data = $this->model->query();

      return DataTables::eloquent($data)
        ->addIndexColumn()
        ->make(true);
    }

    return view($this->view.'index');
  }

  public function create()
  {

  }

  public function store(Request $request)
  {
    DB::beginTransaction();
    try {
      $input = $request->only([
        'name',
      ]);
      
      $data = $this->model->create($input);
      
      $response = [
        'status'  => 'success',
        'message' => "$this->title created successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Created!',
          'text'  => $this->title.' created successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();

      return response()->json($response, 201);
    } catch (Exception $e) {
      DB::rollback();

      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function show($id)
  {
    $data = $this->model->with('books')->findOrFail($id);

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }

  public function edit($id)
  {
    $data = $this->model->findOrFail($id);

    $response = [
      'data' => $data,
    ];

    return response()->json($response, 200);
  }

  public function update(Request $request, $id)
  {
    DB::beginTransaction();
    try {
      $input = $request->only([
        'name',
      ]);
      
      $data = $this->model->findOrFail($id);
  
      $data->update($input);
  
      $response = [
        'status'  => 'success',
        'message' => "$this->title edited successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Edited!',
          'text'  => $this->title.' edited successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();
  
      return response()->json($response, 200);
    } catch (Exception $e) {
      DB::rollback();
    
      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }

  public function destroy($id)
  {
    DB::beginTransaction();
    try {
      $data = $this->model->findOrFail($id);
    
      $data->books()->detach();
      $data->delete();

      $response = [
        'status'  => 'success',
        'message' => "$this->title deleted successfully.",
        'data'    => $data,
        'swal'    => [
          'title' => 'Deleted!',
          'text'  => $this->title.' deleted successfully.',
          'icon'  => 'success',
          'timer' => 3000,
        ],
      ];

      DB::commit();
      
      return response()->json($response, 200);
    } catch (Exception $e) {
      DB::rollback();
    
      abort(500, env('APP_DEBUG') ? $e->getMessage() : 'Something when wrong.');
    }
  }
}
